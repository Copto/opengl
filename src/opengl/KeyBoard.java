package opengl;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.glu.GLU;

public class KeyBoard implements KeyListener {

    private GLCanvas canvas;
    public boolean[] keybuffer;
    //GL gl = drawable.getGL();
    GLU glu = new GLU();//Contiene funciones de mas alto nivel 

    public KeyBoard(GLCanvas canvas) {
        this.canvas = canvas;
        keybuffer = new boolean[256];
    }

    public void keyTyped(KeyEvent e) {
        //System.out.println(e);

        if (keybuffer['w']) {
            opengl.camara.moveForward(-0.25f);
//            LaberintoReady20.avanzar = LaberintoReady20.avanzar - 0.5f;
        }

        if (keybuffer['s']) {
            opengl.camara.moveForward(0.25f);
//            LaberintoReady20.avanzar = LaberintoReady20.avanzar + 0.5f;
        }
        
        if (keybuffer['a']) {
            opengl.camara.rotateY(25);
//            LaberintoReady20.girar = LaberintoReady20.girar + 10;
        }

        if (keybuffer['d']) {
            opengl.camara.rotateY(-25);
//            LaberintoReady20.girar = LaberintoReady20.girar - 10;
        }
        
        if (keybuffer['q']){
            opengl.camara.rotateZ(15);
        }
        
        if (keybuffer['e']){
            opengl.camara.rotateZ(-15);
        }
        
        if (keybuffer['o']){
            opengl.camara.rotateX(15);
        }
        
        if (keybuffer['p']){
            opengl.camara.rotateX(-15);
        }
        
        if (e.getKeyChar() == 'v') {
            if (opengl.mirar == false) {
                opengl.mirar = true;
            } else {
                opengl.mirar = false;
            }
        }

    }

    public void keyPressed(KeyEvent e) {
        keybuffer[e.getKeyChar()] = true;
    }

    public void keyReleased(KeyEvent e) {
        keybuffer[e.getKeyChar()] = false;
    }

}
